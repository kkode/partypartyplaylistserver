#!/usr/bin/env bash
set -eu

tasks/installDependenciesForVlcPlayback.sh
tasks/installYtDlp.sh
tasks/installBuildDependenciesAndBuild.sh
tasks/checkConfigForVlcBackend.sh

echo "Finished with success."
