#!/usr/bin/env bash
set -eu

tasks/installDependenciesForMpdPlayback.sh
tasks/installYtDlp.sh
tasks/installBuildDependenciesAndBuild.sh
tasks/setupMpdDefaultMusicLib.sh
tasks/writeConfigForMpdBackend.sh

echo "Finished with success."
