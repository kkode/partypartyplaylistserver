#!/usr/bin/env bash
set -eu

if [[ $(which http) ]]; then
  songUrl="$1"
  if [[ $(which http) ]]; then
    echo "$songUrl" | http POST localhost:6646/commands/downloadAndEnqueue
    echo "Sent download-and-enqueue command for url $songUrl"
  else
    echo "Please give a valid url as commandline argument to this script."
    return 1
  fi
else
    echo "'http' command not available. Please install package 'httpie' (sudo apt install httpie)."
    return 1
fi

